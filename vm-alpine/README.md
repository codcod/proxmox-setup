# VM setup (Alpine Linux)

## Useful commands

        # rc-status --list
        # rc-status
        # /etc/init.d/networking restart
        # rc-service networking start
        # rc-update add nginx default
        # setup-dns -d localdomain -n 1.1.1.1
        # udhcpc eth0
        # setup-apkrepos -f
        # vi /etc/apk/repositories
        # apk -U upgrade
        # netstat -nplt  # open ports
