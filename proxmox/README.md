# Proxmox configuration details

## Setup networking (proxmox post-install)

        # apt install ./*.deb
        # wpa_passphrase ESSID > /etc/wpa_supplicant/wpa_supplicant.conf
        # systemctl reenable wpa_supplicant.service
        # service wpa_supplicant restart
        # wpa_supplicant -B -i wls3 -c/etc/wpa_supplicant/wpa_supplicant.conf
        # dhclient wls3
        # apt install iwd
        # systemctl --now disable wpa_supplicant
        # apt remove wpasupplicant
        # systemctl --now enable iwd

## IWCtl

        > device list
        > device wls3 show
        > station wls3 scan
        > station wls3 get-networks
        > station wls3 connect ESSID
        > quit

## Post-install config

        # nano /etc/apt/sources.list
        # nano /etc/apt/sources.list.d/pve-enterprise.list  # 
        # ln -s /dev/null /etc/systemd/network/80-iwd.link  # disable renaming wls3 to wlan0

## Useful commands

        # dpkg-reconfigure console-setup
        # systemctl list-units --type=service --state=running

