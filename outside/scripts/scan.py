import concurrent.futures
import configparser
import socket
import typing
import urllib.request


CONFIG_FILE = 'scan.ini'


def config() -> typing.List[typing.Tuple[str, int]]:
    """Read config file (eg. `scan.ini`) and return list of tuples: host, port."""

    config = configparser.ConfigParser()
    config.read(CONFIG_FILE)
    cfg = config['scan']

    port = cfg.getint('port', fallback=80)
    hosts = cfg.get('hosts', fallback=[])

    return [(host, port) for host in hosts.split()]


def _scan_TODO(host:str, port:int):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        try:
            print(f'Trying to connect to {host}:{port}')
            res = s.connect_ex((host, port))
            if res == 0:
                print(f'port {port} on host {host} is opened')
        except OSError as msg:
            print(msg)


def _load_url(url, timeout):
    """Retrieve a single page."""
    with urllib.request.urlopen(url, timeout=timeout) as conn:
        return conn.read()


def scan():
    """Try to retrieve a page from all servers specified in config file."""
    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as pool:
        urls = [f'http://{host}:{port}' for host, port in config()]

        future_to_url = {
            pool.submit(_load_url, url, 2): url for url in urls
        }

        for future in concurrent.futures.as_completed(future_to_url):
            url = future_to_url[future]
            try:
                data = future.result()
            except Exception as exc:
                print('%r generated an exception: %s' % (url, exc))
            else:
                print('%r page is %d bytes' % (url, len(data)))


if __name__ == '__main__':
    scan()

# vim: sw=4:et:ai
